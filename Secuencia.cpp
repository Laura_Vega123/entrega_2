#include<iostream>
#include<string>

// Algoritmo secuencia comun

using namespace std;
int LCS[1024][1024];
int LCSlen(string &x,int x1,string &y,int y1){


    for(int i = 0; i <= x1; i++)
    LCS[i][y1]=0;

    for(int j=0;j<=y1;j++)
    LCS[x1][j]=0;

    for(int i = x1 - 1; i >= 0; i--){
    for(int j = y1 - 1; j >= 0; j--){

    LCS[i][j] = LCS[i+1][j+1];

    if ( i== 0 || j == 0)
     return 0;

    if(x[i] == y[j])
    LCS[i][j]++;

    if(LCS[i][j+1]>LCS[i][j])
    LCS[i][j]=LCS[i][j+1];

    if(LCS[i+1][j]>LCS[i][j])
    LCS[i][j]=LCS[i+1][j];

    }
    }
return LCS[0][0];
}

int main()
{
string x;
string y;
cin >> x >> y;
cout<<endl<<"Length of X[] is = "<<x<<endl;
cout<<endl<<"Length of Y[] is = "<<y<<endl;

int x1 = x.length() , y1 = y.length();
int ans = LCSlen( x, x1, y, y1);
cout<<endl<<"Length of LCS is = "<<LCSlen( x, x1, y, y1)<<endl;
cout << ans << endl;
return 0;
}
