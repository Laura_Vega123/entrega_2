clear all
clc

monedas=[1,2,5,10,20,50];
cambio=228;

cantmonedas=length(monedas);

MP=zeros(cantmonedas,cambio+1);
MR=zeros(cantmonedas,cambio+1);

for i=1:cantmonedas

    MP(i,1)=0;
    MR(i,1)=0;
    
end

for i=1:cantmonedas
    for j=1:cambio
        if ((i==1) && j<monedas(i))
            MR(i,j+1)=inf;
            MP(i,j+1)=0;
        
        else
            if ((i==1) && j>=monedas(i))
                MR(i,j+1)=1+MR(i,j-monedas(i)+1);
                MP(i,j+1)=1;
                
            else
                if((i>1) && j<monedas(i))
                    MR(i,j+1)=MR(i-1,j+1);
                    MP(i,j)=0;
                    
                else
                    MR(i,j+1)= (min(MR(i-1,j+1),1+MR(i,j-monedas(i)+1)));
                    MP(i,j+1)=(MR(i,j+1) ~= MR(i-1,j+1));
       
                end
        
            end

        end
    end
end

i=cantmonedas;
j=cambio;


solucion=zeros(1,cantmonedas);

while (i~= 0 && j+1~=0)
    if(MP(i,j+1)==0)
        i=i-1;
    
    else 
        solucion (i)=solucion(i)+1;
        j=j-monedas(i);
        
    end
    
    %if (i==0)
        %solucion(1)=MR(i,j+1)+monedas(1);
        
   % end
       solucion
       
     
    
    
end